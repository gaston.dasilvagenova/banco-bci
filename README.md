# CRUD Usuario BANCO BCI

Descripción corta del proyecto.

## Tecnologías Utilizadas

- Java
- Spring Boot
- Spring Data JPA
- H2 Database (para desarrollo)
- Spring Security

## Requisitos Previos

- Java 17 
- Maven (para construir y gestionar dependencias) version 3.9.5

## Diagrama de solución
![Diagrama](diagrama.png)

## Configuración del Proyecto

1. **Clonar el Repositorio:**
   ```bash
   git clone https://gitlab.com/gaston.dasilvagenova/banco-bci.git
   cd BCI

2. **Instalacion de dependencias**
   ```bash
   mvn clean install

2. **Correr la aplicacion**
   ```bash
   mvn spring-boot:run

   Tambien pueden usar el run el IDE a eleccion.

# API REST de Usuarios

API REST simple para la gestión de usuarios con autenticación JWT.

## Endpoints Disponibles

### 1. **Crear Usuario**
   - **Método:** `POST`
   - **Ruta:** `api/user/create`
   - **Descripción:** Este endpoint se utiliza para crear un nuevo usuario.
   - **Parámetros de Entrada:**
     ```json
     {
        "name": "name",
        "password": "Password&1",
        "email": "emailTest01@google.cl",
        "phones":[{
            "number":"1",
            "citycode":"23",
            "contrycode": "34"
        }]
     }
     ```
   - **Ejemplo de Uso:**
     ```bash
        curl --location 'http://localhost:8080/api/user/create' \
        --header 'Content-Type: application/json' \
        --header 'Authorization: Basic YWRtaW46YWRtaW4=' \
        --header 'Cookie: JSESSIONID=BB28B72F7CBAECE6B23FCCB435DBD7B7' \
        --data-raw '{
            "name": "name",
            "password": "Password&1",
            "email": "emailTest01@google.cl",
            "phones":[{
                "number":"1",
                "citycode":"23",
                "contrycode": "34"
            }]

        }'
     ```
   - **Respuesta Exitosa:**
     ```json
     {
        "id": "99372804-17f3-44b9-b5d3-0f6ad52ab752",
        "created": "2023-11-26T17:54:39.379+00:00",
        "modified": "2023-11-26T17:54:39.379+00:00",
        "last_login": "2023-11-26T17:54:39.382+00:00",
        "token": null,
        "isactive": true
     }
     ```

### 3. **Obtener Usuario por Email**
   - **Método:** `GET`
   - **Ruta:** `/api/user/read/{email}`
   - **Descripción:** Este endpoint se utiliza para obtener información de un usuario a partir de su email.
   - **Ejemplo de Uso:**
     ```bash
      curl --location 'http://localhost:8080/api/user/read/emailTest01@google.cl' \
      --header 'Authorization: Basic YWRtaW46YWRtaW4=' \
      --header 'Cookie: JSESSIONID=BB28B72F7CBAECE6B23FCCB435DBD7B7'
     ```
   - **Respuesta Exitosa:**
     ```json
     {
        "id": "99372804-17f3-44b9-b5d3-0f6ad52ab752",
        "created": "2023-11-26T17:54:39.379+00:00",
        "modified": "2023-11-26T17:54:39.379+00:00",
        "last_login": "2023-11-26T17:54:39.382+00:00",
        "token": null,
        "isactive": true
     }
     ```

### 4. **Eliminar un Usuario por Email**
   - **Método:** `DELETE`
   - **Ruta:** `/api/user/delete/{email}`
   - **Descripción:** Este endpoint se utiliza para eliminar un usuario a partir de su email.
   - **Ejemplo de Uso:**
     ```bash
      curl --location --request DELETE 'http://localhost:8080/api/user/delete/emailTest01@google.cl' \
      --header 'Authorization: Basic YWRtaW46YWRtaW4=' \
      --header 'Cookie: JSESSIONID=BB28B72F7CBAECE6B23FCCB435DBD7B7'     
     ```
   - **Respuesta Exitosa:**
     ```json
     {
       "Usuario eliminado"
     }
     ```

### 5. **Actualizar Usuario**
   - **Método:** `PATCH`
   - **Ruta:** `/api/user/update/{email}`
   - **Descripción:** Este endpoint se utiliza para actualizar información de un usuario a partir de su email.
   - **Parámetros de Entrada:**
     ```json
     {
        "name": "actualizado",
        "password": "Password&1",
        "phones":[{
            "number":"12",
            "citycode":"67",
            "contrycode": "47"
        }]
     }
     ```
   - **Ejemplo de Uso:**
     ```bash
     curl --location --request PATCH 'http://localhost:8080/api/user/update/emailTest01@google.cl' \
        --header 'Content-Type: application/json' \
        --header 'Authorization: Basic YWRtaW46YWRtaW4=' \
        --header 'Cookie: JSESSIONID=BB28B72F7CBAECE6B23FCCB435DBD7B7' \
        --data '{
            "name": "actualizado",
            "password": "Password&1",
            "phones":[{
                "number":"12",
                "citycode":"67",
                "contrycode": "47"
            }]

        }'
     ```
   - **Respuesta Exitosa:**
     ```json
     {
      "id": "07cb3a3c-744b-4a12-9d23-f36f1556a4d2",
      "created": "2023-11-26T18:33:30.555+00:00",
      "modified": "2023-11-26T18:33:39.496+00:00",
      "last_login": "2023-11-26T18:33:39.518+00:00",
      "token": null,
      "isactive": true
     }
     ```

## Notas Adicionales
- Los endpoints protegidos requieren el basic auth (`Authorization: Basic YWRtaW46YWRtaW4=`) para la autenticación. se puede obtener agregando un basic auth al header, en postman se hace agregando un basic auth username = admin y password = admin



