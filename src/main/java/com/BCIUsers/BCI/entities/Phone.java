package com.BCIUsers.BCI.entities;


import java.util.UUID;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Phone {
    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;
    private String number;
    private String citycode;
    private String contrycode;
}
