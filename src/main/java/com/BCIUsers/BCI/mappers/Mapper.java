package com.BCIUsers.BCI.mappers;


import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.BCIUsers.BCI.entities.Phone;
import com.BCIUsers.BCI.entities.Usuario;
import com.BCIUsers.BCI.interfaces.PhoneRequest;
import com.BCIUsers.BCI.interfaces.UserRequest;
import com.BCIUsers.BCI.interfaces.UserResponse;
import com.BCIUsers.BCI.security.TokenUtils;

public class Mapper {


    public List<Phone> getPhoneEntitiesFromPhoneRequest(List<PhoneRequest> phoneRequest) {
        if(phoneRequest!= null){
            return phoneRequest.stream().map(p -> {
            Phone phone = new Phone();
            phone.setCitycode(p.getCitycode());
            phone.setContrycode(p.getContrycode());
            phone.setNumber(p.getNumber());
            return phone;
        }).collect(Collectors.toList());
        }
        return null;
    }

    public UserResponse getUserResponse(Usuario usuario){
        UserResponse userRes = new UserResponse();
        userRes.setCreated(usuario.getCreateAt());
        userRes.setCreated(usuario.getCreateAt());
        userRes.setId(usuario.getId().toString());
        userRes.setModified(usuario.getModified());
        userRes.setIsactive(true);
        userRes.setLast_login(new Date());
        return userRes;
    }

    public Usuario getUsuarioEntenty(UserRequest userRequest){
        List<Phone> listPhones = getPhoneEntitiesFromPhoneRequest(userRequest.getPhones());
        Usuario usuario = new Usuario();
        usuario.setName(userRequest.getName());
        usuario.setPassword((userRequest.getPassword()));
        usuario.setEmail(userRequest.getEmail());
        usuario.setPhones(listPhones);
        return usuario;
    }
}
