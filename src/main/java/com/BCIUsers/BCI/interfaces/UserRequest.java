package com.BCIUsers.BCI.interfaces;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserRequest {
    private String name;
    private String password;
    private String email;
    private List<PhoneRequest> phones;
}
