package com.BCIUsers.BCI.interfaces;

import lombok.Data;

@Data
public class PhoneRequest {
    private String number;
    private String citycode;
    private String contrycode;
}
