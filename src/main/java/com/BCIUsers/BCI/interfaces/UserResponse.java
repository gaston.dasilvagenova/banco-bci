package com.BCIUsers.BCI.interfaces;

import java.util.Date;
import lombok.Data;

@Data
public class UserResponse {
    private String id ;
    private Date created ;
    private Date modified ;
    private Date last_login ;
    private String token ;
    private Boolean isactive ;

}
