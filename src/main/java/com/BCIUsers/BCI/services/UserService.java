package com.BCIUsers.BCI.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.BCIUsers.BCI.entities.Usuario;
import com.BCIUsers.BCI.exceptions.CustomValidationException;
import com.BCIUsers.BCI.interfaces.UserRequest;
import com.BCIUsers.BCI.interfaces.UserResponse;
import com.BCIUsers.BCI.mappers.Mapper;
import com.BCIUsers.BCI.repositories.UserRepository;

import java.util.Date;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository usuarioRepository;

    @Value("${security.password.regex}")
    private String passwordRegex;
    @Value("${security.email.regex}")
    private String emailRegex;

    private Mapper mapper = new Mapper();
    
    // Método para eliminar un usuario por su ID
    public void deleteUserByEmail(String email) {
        Usuario usuarioExistente = validateAndGetUser(email);
        usuarioRepository.delete(usuarioExistente);
    }

    // Método para actualizar un usuario existente
    public UserResponse updateUser(String email, UserRequest userRequest) {
        // Lógica de validación y obtenció del usuario
        Usuario usuarioExistente = validateAndGetUser(email);

        // Aplicar actualizaciones al usuario existente
        usuarioExistente.setModified(new Date());
        if(userRequest.getPassword()!=null){
            usuarioExistente.setPassword(userRequest.getPassword());
        }
        if(userRequest.getName()!=null){
            usuarioExistente.setName(userRequest.getName());
        }

        if(userRequest.getPhones()!= null){
            usuarioExistente.getPhones().clear();
            usuarioExistente.getPhones().addAll(mapper.getPhoneEntitiesFromPhoneRequest(userRequest.getPhones()));
        }
        usuarioRepository.save(usuarioExistente);
        return mapper.getUserResponse(usuarioExistente);
    }

    public UserResponse getUserByEmail(String email) {
        Usuario usuarioExistente = validateAndGetUser(email);
        return mapper.getUserResponse(usuarioExistente);
    }


    // Método para crear un nuevo usuario
    public UserResponse validateAndSaveUser(UserRequest userRequest) {
        validateData(userRequest);
        try{
            Usuario userPersisted = usuarioRepository.save(mapper.getUsuarioEntenty(userRequest));
            return mapper.getUserResponse(userPersisted);
        }catch(Error err){
            System.out.println(err);
            throw err;
        }
    }

    // Método para obtener un usuario por su Email
    private Usuario validateAndGetUser(String email){
        Optional<Usuario> usuarioExistente = usuarioRepository.findByEmail(email);
        if (!usuarioExistente.isPresent()) {
            throw new CustomValidationException(HttpStatus.NOT_FOUND,"El usuario no existe con este email");
        }
        return usuarioExistente.get();
    }

    // Método de validación de data del usuario
    private void validateData(UserRequest userRequest) {
        validatePassword(userRequest.getPassword());
        validateEmail(userRequest.getEmail());
        ValidateExistedEmail(userRequest.getEmail());
    }

   
    // Método de validación de password
    private void validatePassword(String password) {
        if (!password.matches(passwordRegex)) {
            throw new CustomValidationException(HttpStatus.UNPROCESSABLE_ENTITY,"El formato del password no cumple con los requisitos");
        }
    }

    // Método de validación de email
    private void validateEmail(String email) {
        if(!email.matches(emailRegex)){
            throw new CustomValidationException(HttpStatus.UNPROCESSABLE_ENTITY,"El formato del email no cumple con los requisitos");
        }
    }

    // Método de validación para ver si existe el usuario en la base de datos.
    private void ValidateExistedEmail(String email) {
        Optional<Usuario> usuarioExistente = usuarioRepository.findByEmail(email);
        if (usuarioExistente.isPresent()) {
            throw new CustomValidationException(HttpStatus.UNPROCESSABLE_ENTITY,"Ya existe un usuario con este email");
        }
    }
}
