package com.BCIUsers.BCI.security;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenUtils {
    private final static String ACCESS_TOKEN_SECRET = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9";
    private final static Long ACCESS_TOKEN_TTL = 2_598_458L;


    public static String createToken(String nombre,String email){
        Long expirationTime = ACCESS_TOKEN_TTL * 1_000;
        Date expirationDate = new Date(System.currentTimeMillis() + expirationTime );
        Map<String,Object> extra = new HashMap<>();
        extra.put("nombre", nombre);

        return Jwts.builder()
            .setSubject(email)
            .setExpiration(expirationDate)
            .addClaims(extra)
            .signWith(SignatureAlgorithm.HS256, ACCESS_TOKEN_SECRET.getBytes())
            .compact();
    }
}
