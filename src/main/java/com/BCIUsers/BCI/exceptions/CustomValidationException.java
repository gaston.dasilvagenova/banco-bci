package com.BCIUsers.BCI.exceptions;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class CustomValidationException extends RuntimeException {
     private final HttpStatus httpStatus;
    private final String message;

    public CustomValidationException(HttpStatus httpStatus, String message) {
        super(message);
        this.httpStatus = httpStatus;
        this.message = message;
    }
}

