package com.BCIUsers.BCI;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.BCIUsers.BCI.entities.Usuario;
import com.BCIUsers.BCI.exceptions.CustomValidationException;
import com.BCIUsers.BCI.interfaces.UserRequest;
import com.BCIUsers.BCI.interfaces.UserResponse;
import com.BCIUsers.BCI.repositories.UserRepository;
import com.BCIUsers.BCI.services.UserService;

@SpringBootTest
public class UserServiceTests {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    String emailPrueba = "emailTest01@google.cl";


    @Test
    public void testCreateUser() {
        // Datos de prueba
        UserRequest userRequest = new UserRequest("name", "Password&1", emailPrueba, null);
        
        // Mock del repositorio para simular getUserByEmail
        when(userRepository.findByEmail(emailPrueba)).thenReturn(Optional.empty()); // Simula que el usuario no existe

        // Mock del repositorio para simular el comportamiento
        when(userRepository.save(any(Usuario.class))).thenAnswer(invocation -> {
            Usuario savedUser = invocation.getArgument(0);
            savedUser.setId(new UUID(1, 2)); // Simula la asignación de un ID por el repositorio
            return savedUser;
        });

        // Llamada al método del servicio
        UserResponse createdUser = userService.validateAndSaveUser(userRequest);

        // Verificación de que el repositorio.save fue llamado con el usuario correcto
        verify(userRepository, times(1)).save(any(Usuario.class));

        // Asserts para verificar que el usuario creado tiene el ID asignado y los datos correctos
        assertNotNull(createdUser.getId());
        assertEquals(true, createdUser.getIsactive());
    }

    @Test
    public void testCreateUser_UserAlreadyExists() {

        // Datos de prueba
        UserRequest userRequest = new UserRequest("name", "Password&1", emailPrueba, null);
        
        // Mock del repositorio para simular getUserByEmail
        when(userRepository.findByEmail(emailPrueba)).thenReturn(Optional.of(createMockUser())); // Simula que el usuario existe

         // Llamada al método del servicio y esperamos una excepción
        assertThrows(CustomValidationException.class, () -> userService.validateAndSaveUser(userRequest));

        // Verificación de que getUserByEmail se llamó con el email correcto
        verify(userRepository, times(1)).findByEmail(emailPrueba);
    }

    @Test
    public void testGetUsuario() {

        // Mock del repositorio para simular el comportamiento del findByEmail
        when(userRepository.findByEmail(emailPrueba)).thenReturn(Optional.of(createMockUser())); // Simula que el usuario existe

        // Llamada al método del servicio
        UserResponse foundUser = userService.getUserByEmail(emailPrueba);

        // Verificación de que findByEmail se llamó con el email correcto
        verify(userRepository, times(1)).findByEmail(emailPrueba);

        // Asserts para verificar que se encontró el usuario
        assertNotNull(foundUser.getId());
        assertEquals(true, foundUser.getIsactive());
    }

    @Test
    public void testDeleteUser_Success() {

        Usuario userPersisted = createMockUser();
        // Mock del repositorio para simular el comportamiento del findByEmail
        when(userRepository.findByEmail(emailPrueba)).thenReturn(Optional.of(userPersisted)); // Simula que el usuario existe

        // Mock del repositorio para simular el comportamiento del deleteById
        doNothing().when(userRepository).delete(createMockUser());

        // Llamada al método del servicio
        userService.deleteUserByEmail(emailPrueba);

        // Verificación de que delete se llamó correctamente.
        verify(userRepository, times(1)).delete(userPersisted);
    }

    @Test
    public void testDeleteUser_UserNotExist() {
        // Mock del repositorio para simular el comportamiento del findByEmail
        when(userRepository.findByEmail(emailPrueba)).thenReturn(Optional.empty()); // Simula que el usuario existe

         // Llamada al método del servicio y esperamos una excepción
        assertThrows(CustomValidationException.class, () -> userService.deleteUserByEmail(emailPrueba));

        // Verificación de que findByEmail se llamó con el email correcto
        verify(userRepository, times(1)).findByEmail(emailPrueba);
    }

    @Test
    public void testUpdateUser_Success() {
        // Datos de prueba
        UserRequest updateRequest = new UserRequest("actualizado","Password&2",emailPrueba,null);

        // Mock del repositorio para simular el comportamiento del save
        when(userRepository.findByEmail(emailPrueba)).thenReturn(Optional.of(createMockUser())); // Simula que el usuario existe
        when(userRepository.save(any(Usuario.class))).thenAnswer(invocation -> invocation.getArgument(0)); // Simula el guardado exitoso

        // Llamada al método del servicio
        userService.updateUser(emailPrueba, updateRequest);

        // Verificación de que findByEmail se llamó con el email correcto
        verify(userRepository, times(1)).findByEmail(emailPrueba);

        // Verificación de que el repositorio.save fue llamado con el usuario actualizado
        verify(userRepository, times(1)).save(any(Usuario.class));
    }

    @Test
    public void testUpdateUser_UserNotExist() {
        // Datos de prueba
        UserRequest updateRequest = new UserRequest("actualizado","Password&2",emailPrueba,null);

        // Mock del repositorio para simular el comportamiento del findByEmail
        when(userRepository.findByEmail(emailPrueba)).thenReturn(Optional.empty()); // Simula que el usuario existe

        // Llamada al método del servicio y esperamos una excepción
        assertThrows(CustomValidationException.class, () -> userService.updateUser(emailPrueba, updateRequest));

        // Verificación de que findByEmail se llamó con el email correcto
        verify(userRepository, times(1)).findByEmail(emailPrueba);

        // Verificación de que el repositorio.save fue llamado con el usuario actualizado
        verify(userRepository, times(0)).save(any(Usuario.class));
    }

     // Método auxiliar para crear un usuario de prueba
     private Usuario createMockUser() {
        Usuario user = new Usuario();
        user.setId(new UUID(1, 2));
        user.setName("nuevo-usuario");
        user.setPassword("nueva-contraseña");
        user.setEmail("nuevo@dominio.cl");
        return user;
    }
}
